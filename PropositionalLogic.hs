module PropositionalLogic
where

import Data.List

import Prelude hiding (putStr, putStrLn)
import Data.ByteString.Char8 (putStr, putStrLn)
import Data.ByteString.UTF8 (fromString)

-----------------------------------------------------------
--
-- Definition of syntax
--
-----------------------------------------------------------

-- Definition of data type Plf
-- ##################
-- # YOUR CODE HERE #
-- ##################
-- END CODE SNIPPET

{-
Example for data type Plf.
-- BEGIN CODE SNIPPET: "ExamplePlf.hs"
> :t ((At "x") `And` (At "z"))
     `Or` ((At "y") `And` (Not (At "z")))
< Or (And (At "x") (At "z"))
     (And (At "y") (Not (At "z"))) :: Plf
-- END CODE SNIPPET
-}

-- Pretty Print
-- BEGIN CODE SNIPPET: "PrettyPrint.hs"
pp :: Plf -> IO ()
pp f = putStrLn $ fromString $ pp' f
  where
  pp' :: Plf -> String
  pp' (Or a b) =
    "(" ++ (pp' a) ++ " ∨ " ++ (pp' b) ++ ")"
  pp' (And a b) =
    "(" ++ (pp' a) ++ " ∧ " ++ (pp' b) ++ ")"
  pp' (Not a) =
    "(¬ " ++ (pp' a) ++ ")"
  pp' (Cond a b) =
    "(" ++ (pp' a) ++ " → " ++ (pp' b) ++ ")"
  pp' (Bicond a b) =
    "(" ++ (pp' a) ++ " ↔ " ++ (pp' b) ++ ")"
  pp' (At a) = a
-- END CODE SNIPPET

{-
Example for pretty printing
> pp $ (Not (At "a") `Or` (And (At "b") (At "c")))
< ((¬ a) ∨ (b ∧ c))
-}

-----------------------------------------------------------
--
-- Definition of semantics
--
-----------------------------------------------------------

-- Valuation of atoms of PL formulas
emptyValuation :: Show a => a -> b
-- BEGIN CODE SNIPPET: "EmptyValuation.hs"
emptyValuation =
  (\x -> error 
    ("Referencing undefined variable \"" ++ (show x) ++ "\"."))
-- END CODE SNIPPET

-- Set valuation of atom
set :: Eq a => a -> b -> (a -> b) -> (a -> b)
-- BEGIN CODE SNIPPET: "Set.hs"
set a b v = (\x -> if x == a then b else (v x))
-- END CODE SNIPPET

-- Set valuations of list of atoms
setAll :: (Show a, Eq a) => (a -> b) -> [(a,b)] -> (a -> b)
-- BEGIN CODE SNIPPET: "SetAll.hs"
setAll = foldr (\(a, x) v -> (set a x v))
-- END CODE SNIPPET

{-
Example for variable valuation.
-- BEGIN CODE SNIPPET: "ExampleVariableValuation.hs"
> (setAll emptyValuation [("x", True), ("y", False)]) "x"
< True
> (setAll emptyValuation [("x", True), ("y", False)]) "y"
< False
-- END CODE SNIPPET
-}

-- Evaluation of PL formulas
eval :: Plf -> (String -> Bool) -> Bool
-- ##################
-- # YOUR CODE HERE #
-- ##################
eval = undefined
-- END CODE SNIPPET

{-
Example for valuation.
-- BEGIN CODE SNIPPET: "ExampleFormulaValuation.hs"
> eval ((At "x") `And` (At "y"))
       (setAll emptyValuation [("x", True), ("y", False)])
< False
> eval ((At "x") `Or` (At "y"))
       (setAll emptyValuation [("x", True), ("y", False)])
< True
-- END CODE SNIPPET
-}

-----------------------------------------------------------
--
-- Tautology, (Un-)Satisfiability
--
-----------------------------------------------------------

-- Function allAtoms extracts all atoms from a formula.
-- This will come handy for the following functions.
allAtoms :: Plf -> [String]
-- BEGIN CODE SNIPPET: "AllAtoms.hs"
allAtoms f = nub (allAtoms' f)
  where
  allAtoms' (At a) = [a]
  allAtoms' (Not a) = allAtoms' a
  allAtoms' (And a b) = (allAtoms' a) ++ (allAtoms' b)
  allAtoms' (Or a b) = (allAtoms' a) ++ (allAtoms' b)
  allAtoms' (Cond a b) = (allAtoms' a) ++ (allAtoms' b)
  allAtoms' (Bicond a b) = (allAtoms' a) ++ (allAtoms' b)
-- END CODE SNIPPET

-- Test if formula f is a tautology
tautology :: Plf -> Bool
-- ##################
-- # YOUR CODE HERE #
-- ##################
tautology = undefined
-- END CODE SNIPPET

{-
Example for tautology checking.
-- BEGIN CODE SNIPPET: "ExampleTautology.hs"
> tautology $ ((At "x") `Or`  (At "y"))
              `Or` ((Not (At "x")) `And` (Not (At "y")))
< True
> tautology $ ((At "x") `And` (At "y"))
              `Or` ((Not (At "x")) `Or`  (Not (At "y")))
< True
> tautology $ ((At "x") `And` (At "y"))
              `Or` ((Not (At "x")) `And` (Not (At "y")))
< False
-- END CODE SNIPPET
-}

-- Notice how we use both Not (defined in our object logic) and not
-- (defined in our meta logic) in the following definitions.
unsatisfiable :: Plf -> Bool
satisfiable :: Plf -> Bool
-- BEGIN CODE SNIPPET: "Satisfiable.hs"
unsatisfiable f = tautology (Not f)
satisfiable f = not (unsatisfiable f)
-- END CODE SNIPPET

-----------------------------------------------------------
--
-- Substitution
--
-----------------------------------------------------------

subst :: Plf -> Plf -> Plf -> Plf
-- ##################
-- # YOUR CODE HERE #
-- ##################
subst = undefined
-- END CODE SNIPPET

{-
Substitution example
-- BEGIN CODE SNIPPET: "ExampleSubstitution.hs"
> pp $ subst (((At "x") `Or` (At "y")) `And` (At "z"))
             ((At "x") `Or` (At "y"))
             (Not ((Not (At "x")) `And` (Not (At "y"))))
< ((¬ ((¬ x) ∧ (¬ y))) ∧ z)
-- END CODE SNIPPET
-}
