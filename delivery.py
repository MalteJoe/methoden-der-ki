#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""A logical agent navigating Wumpus world."""

from __future__ import print_function
import pycosat
import sys
import pdb
import logging

#################################################################
#
# Functions for interfacing PicoSAT
#
#################################################################

atom_list = []

def s2n(s):
    """Convert string representation of atom to number."""
    if(s[0] == '-'):
        inv = -1
        t = s[1:]
    else:
        inv = 1
        t = s
    try:
        n = atom_list.index(t)
    except:
        # Atom not in list. Add it.
        atom_list.append(t)
        n = len(atom_list) - 1
    return((n+1) * inv)

def n2s(n):
    """Convert numeric representation of atom to string."""
    r = atom_list[abs(n)-1]
    if (n) < 0:
        r = '-' + r
    return(r)

def solve(axioms):
    picosatInput = map(lambda c: map(lambda s: s2n(s), c), axioms)
    picosatSolution = pycosat.solve(picosatInput)
    if(picosatSolution == 'UNSAT'):
        return(picosatSolution)
    else:
        return(map(lambda atom: n2s(atom), picosatSolution))

def print_actions_in(clause):
    # Print all positive actions in clause.
    action_trace = []
    for c in clause:
        for a in actions:
            if (c.find(a) == 0):
                action_trace += [c]
    for a in action_trace[:-1]:
        logging.info(a)


def print_atom_trace_by_step(solution):
    solution_by_step = [[] for _ in xrange(0, t_max+1)]
    for c in solution:
        solution_by_step[int(c[-2:])] += [c[:-3]]
    #print(solution_by_step)
    logging.debug("\n\n=== TRACE ===")
    for c in filter(lambda c: c[0]!='-', solution_by_step[0]):
        logging.debug(c)
    for i in xrange(1, t+1):
        logging.debug("\n=== Step %d ===" % i)
        for step in filter(lambda x: x not in solution_by_step[i-1], solution_by_step[i]):
            logging.debug(step)


#################################################################
#
# Axioms about the world
#
#################################################################

actions = ['north', 'south', 'west', 'east', 'pick_up', 'drop', 'unlock', 'install_key']

# Movement axioms

def movement_axioms(t):
    """All movmement axioms for time t."""
    # Floor plan
    # Format: Location,
    #         Location North, Location East, Location South, Location West
    floorplan = [
          ['main_office',
           'mail_drop', 'main_office', 'main_office', 'main_office']
        , ['mail_drop',
           'mail_drop', 'ts', 'main_office', 'mail_drop']
        , ['ts',
           'a2', 'o101', 'ts', 'mail_drop']
        , ['storage',
           'storage', 'storage', 'o119', 'o119']

        # Hallways
        , ['o101',
           'a3', 'o103', 'r101', 'ts']
        , ['o103',
           'b3', 'o105', 'r103', 'o101']
        , ['o105',
           'o105', 'o107', 'r105', 'o103']
        , ['o107',
           'b4', 'o107', 'r107', 'o105']
        , ['o109',
           'o113', 'o111', 'r109', 'o107']
        , ['o111',
           'o111', 'o111', 'r111', 'o109']
        , ['o113',
           'o115', 'r113', 'o109', 'o113']
        , ['o115',
           'o117', 'r115', 'o113', 'o115']
        , ['o117',
           'o119', 'r117', 'o115', 'o119']
        , ['o119',
           'r119', 'storage', 'o117', 'o121']
        , ['o121',
           'r121', 'o119', 'o121', 'o123']
        , ['o123',
           'r123', 'o121', 'c1', 'o125']
        , ['o125',
           'r125', 'o123', 'd2', 'o127']
        , ['o127',
           'r127', 'o125', 'o125', 'o129']
        , ['o129',
           'r129', 'o127', 'd1', 'o131']
        , ['o131',
           'r131', 'o129', 'o131', 'o131']

        # Rooms
        , ['r101',
           'o101', 'r101', 'r101', 'r101']
        , ['r103',
           'o103', 'r103', 'r103', 'r103']
        , ['r105',
           'o105', 'r105', 'r105', 'r105']
        , ['r107',
           'o107', 'r107', 'r107', 'r107']
        , ['r109',
           'o109', 'r109', 'r109', 'r109']
        , ['r111',
           'o111', 'r111', 'r111', 'r111']
        , ['r113',
           'r113', 'r113', 'r113', 'o113']
        , ['r115',
           'r115', 'r115', 'r115', 'o115']
        , ['r117',
           'r117', 'r117', 'r117', 'o117']
        , ['r119',
           'r119', 'r119', 'o119', 'r119']
        , ['r121',
           'r121', 'r121', 'o121', 'r121']
        , ['r123',
           'r123', 'r123', 'o123', 'r123']
        , ['r125',
           'r125', 'r125', 'o125', 'r125']
        , ['r127',
           'r127', 'r127', 'o127', 'r127']
        , ['r129',
           'r129', 'r129', 'o129', 'r129']
        , ['r131',
           'r131', 'r131', 'o131', 'r131']
        
        # Lab complex A
        , ['a1',
           'd3', 'b1', 'a3', 'a1']
        , ['a2',
           'a2', 'a3', 'ts', 'a2']
        , ['a3',
           'a1', 'a3', 'o101', 'a2']
        
        # Lab complex B
        , ['b1',
           'c2', 'b2', 'b3', 'a1']
        , ['b2',
           'c3', 'b2', 'b4', 'b1']
        , ['b3',
           'b1', 'b4', 'o103', 'b3']
        , ['b4',
           'b2', 'b4', 'o107', 'b3']

        # Lab complex C
        , ['c1',
           'o123', 'c3', 'c2', 'c1']
        , ['c2',
           'c1', 'c3', 'b1', 'c2']
        , ['c3',
           'c1', 'c3', 'b2', 'c2']

        # Lab complex D
        , ['d1',
           'o129', 'd2', 'd3', 'd1']
        , ['d2',
           'o125', 'd2', 'd3', 'd1']
        , ['d3',
           'd2', 'd3', 'a1', 'd1']
        ]

    # Movement axioms
    locations = [l[0] for l in floorplan]
    room_links = []
    
    for l in floorplan:
        room = l[0]
            
        for (direction, connection) in zip(['north', 'east', 'south', 'west'],
                                           l[1:]):
            
            # Rooms may be locked
            if (connection[0]=='r'):
                # room_t ∧ locked_connection_t ∧ direction_t → room_t+1
                room_links += [['-robot_at_%s_%02d' % (room, t),
                                '-%s_%02d' % (direction, t),
                                '-locked_%s_%02d' % (connection, t),
                                'robot_at_%s_%02d' % (room, t+1)]]
                # room_t ∧ ¬locked_connection_t ∧ direction_t → connection_t+1
                room_links += [['-robot_at_%s_%02d' % (room, t),
                                '-%s_%02d' % (direction, t),
                                'locked_%s_%02d' % (connection, t),
                                'robot_at_%s_%02d' % (connection, t+1)]]
                
            else:
                # room_t ∧ direction_t → connection_t+1
                room_links += [['-robot_at_%s_%02d' % (room, t),
                                '-%s_%02d' % (direction, t),
                                'robot_at_%s_%02d' % (connection, t+1)]]
    
    # Picking up and dropping mail does not change the location.
    # robot_at_main_office_t ∧ pick_up_t → robot_at_main_office_t+1
    # robot_at_main_office_t ∧ drop_t → robot_at_main_office_t+1
    no_robot_move_picking_dropping = []
    for l in locations:
        for a in ['-pick_up_%02d' % t, '-drop_%02d' % t, '-unlock_%02d' % t, '-install_key_%02d' % t]:
            no_robot_move_picking_dropping += [['-robot_at_%s_%02d' % (l, t), a,
                                                'robot_at_%s_%02d' % (l, t+1)]]
    
    # There is exactly one robot
    
    # (¬ robot_at_main_office_t ∨ ¬ robot_at_mail_drop_t) ∧ 
    # (¬ robot_at_main_office_t ∨ ¬ robot_at_o101_t) ∧ 
    # (¬ robot_at_mail_drop_t ∨ ¬ robot_at_o101_t) ∧ 
    # ...
    only_one_robot = []
    only_one_robot += [['robot_at_%s_%02d' % (l, t) for l in locations]]
    for l0 in xrange(len(locations)):
        for l1 in xrange(l0+1, len(locations)):
            only_one_robot += [['-robot_at_%s_%02d' % (locations[l0], t),
                                '-robot_at_%s_%02d' % (locations[l1], t),]]

    # Exactly one action at a time:
    one_action_a_time = []
    # At least one action at a time
    # north_t ∨ south_t ∨ west_t ∨ east_t ∨ pick_up_t ∨ drop_t
    one_action_a_time += [['%s_%02d' % (l, t) for l in actions]]
    # At most one action at a time
    for a0 in xrange(len(actions)):
        for a1 in xrange(a0+1, len(actions)):
            if (a0 != a1):
                one_action_a_time += [['-%s_%02d' % (actions[a0], t),
                                       '-%s_%02d' % (actions[a1], t)]]

    # Picking up and dropping mail
    pick_drop_mail = []
    for l in locations:
        # pick_up_t → robot_carries_mail_t+1
        pick_drop_mail += [['-pick_up_%02d'  % t,
                            'robot_carries_mail_%02d'  % (t+1)]]
        # drop_t → ¬ robot_carries_mail_t+1
        pick_drop_mail += [['-drop_%02d' % t, '-robot_carries_mail_%02d' % (t+1)]]
        # ¬ robot_carries_mail_t ∧ ¬ pick_up_t → ¬ robot_carries_mail_t+1
        pick_drop_mail += [['robot_carries_mail_%02d' % t, 'pick_up_%02d' % t,
                    '-robot_carries_mail_%02d' % (t+1)]]
        # robot_carries_mail_t ∧ ¬ drop_t → robot_carries_mail_t+1
        pick_drop_mail += [['-robot_carries_mail_%02d' % t, 'drop_%02d' % t,
                    'robot_carries_mail_%02d' % (t+1)]]

    #  ¬(pick_up_t ∧ robot_at_mail_office_t ∧ mail_at_mail_drop_t) ∧
    #  ¬(pick_up_t ∧ robot_at_mail_office_t ∧ mail_at_ts_t) ∧
    #  ...
    #  ¬(pick_up_t ∧ robot_at_mail_drop_t ∧ mail_at_mail_ts_t) ∧
    #  ...
    for l0 in locations:
        for l1 in locations:
            if (l0 != l1):
                pick_drop_mail += [['-pick_up_%02d' % t,
                                    '-robot_at_%s_%02d' % (l0, t),
                                    '-mail_at_%s_%02d' % (l1, t)]]

    # Moving mail
    move_mail = []
    for l in locations:
        # ¬ robot_carries_mail_t ∧ mail_at_l_t → mail_at_l_t+1
        move_mail += [['robot_carries_mail_%02d' % t,
                       '-mail_at_%s_%02d' % (l, t),
                       'mail_at_%s_%02d' % (l, t+1)]]
        # robot_carries_mail_t ∧ robot_at_l_t+1 → mail_at_l_t+1
        move_mail += [['-robot_carries_mail_%02d' % t,
                       '-robot_at_%s_%02d' % (l, t+1), 'mail_at_%s_%02d' % (l, t+1)]]


    # There is only one piece of mail
    # (¬ mail_at_main_office_t ∨ ¬ mail_at_mail_drop_t) ∧ 
    # (¬ mail_at_main_office_t ∨ ¬ mail_at_o101_t) ∧ 
    # (¬ mail_at_mail_drop_t ∨ ¬ mail_at_o101_t) ∧ 
    # ...
    one_piece_mail = []
    for l0 in xrange(len(locations)):
        for l1 in xrange(l0+1, len(locations)):
            one_piece_mail += [['-mail_at_%s_%02d' % (locations[l0], t),
                                '-mail_at_%s_%02d' % (locations[l1], t)]]
    
        
    ######### LOCKING ROOMS WITH KEYS #################
    
    room_unlocks = []
    doors_stay_shut = []
    anti_auto_install = []
    key_installs = []
    unique_keys = []
    for l in filter(lambda l: l[0] == 'r', locations):
        # Unlocking rooms
        # room_t ∧ key_room_t ∧ unlock_t → ¬locked_room_t+1
        room_unlocks += [['-robot_at_o%s_%02d' % (l[1:], t),
                          '-robot_has_key_%s_%02d' % (l, t),
                          '-unlock_%02d' % t,
                          '-locked_%s_%02d' % (l, t+1)]]
                          
        # Doors stay shut if not unlocked or we don't have the key
        # ¬unlock_t ∧ locked_room_t → locked_room_t+1
        # ¬key_room_t ∧ locked_room_t → locked_room_t+1
        # ¬floor_room_t ∧ locked_room_t → locked_room_t+1
        for a in ['unlock_%02d' % t, 'robot_has_key_%s_%02d' % (l, t), 'robot_at_o%s_%02d' % (l[1:], t)]:
            doors_stay_shut += [[a, '-locked_%s_%02d' % (l, t),
                                 'locked_%s_%02d' % (l, t+1)]]
        
        # The robot doesn't lose keys
        # key_room_t → key_room_t+1
        #key_installs += [['-robot_has_key_%s_%02d' % (l, t),
         #                 'robot_has_key_%s_%02d' % (l, t+1)]]
        
    for (l0, l) in enumerate(locations):
        # Installing/Downloading keys
        for r in filter(lambda l2: l2[0] == 'r', locations):
            # install_key_t ∧ robot_at_location_t ∧ key_room_in_location → key_room_t+1
            key_installs += [['-robot_at_%s_%02d' % (l, t),
                              '-install_key_%02d' % t,
                              '-key_%s_in_%s_00' % (r, l),
                              'robot_has_key_%s_%02d' % (r, t+1)]]
            
            # install_key_t → key_room_t+1
            #key_installs += [['-install_key_%02d' % t,
            #                  'robot_has_key_%s_%02d' % (r, t+1)]]
            # ¬ robot_has_key_room_t ∧ ¬ install_key_t → ¬ robot_has_key_room_t+1
            key_installs += [['robot_has_key_%s_%02d' % (r, t),
                              'install_key_%02d' % t,
                              '-robot_has_key_%s_%02d' % (r, t+1)]]
            
            # ¬ robot_has_key_room_t ∧ ¬ robot_at_location_t ∧ ¬ key_room_in_location_00 → ¬ robot_has_key_room_t+1
            key_installs += [['robot_has_key_%s_%02d' % (r, t),
                              'install_key_%02d' % t,
                              'key_%s_in_%s_00' % (r, l),
                              'robot_at_%s_%02d' % (l, t),
                              '-robot_has_key_%s_%02d' % (r, t+1)]]
            
            for l2 in filter(lambda loc: loc != l, locations):
                # robot_at_location_t ∧ ¬robot_has_key_room_t ∧ key_room_in_other_location_00 → ¬robot_has_key_room_t+1
                key_installs += [['-robot_at_%s_%02d' % (l, t),
                                  'robot_has_key_%s_%02d' % (r, t),
                                  '-key_%s_in_%s_00' % (r, l2),
                                  '-robot_has_key_%s_%02d' % (r, t+1)]]
                                  
            for l1 in xrange(l0+1, len(locations)):
                # All keys are unique
                # (¬ key_r101_in_main_office ∨ ¬ key_r101_in_mail_drop)
                # ...
                unique_keys += [['-key_%s_in_%s_00' % (r, l),
                                 '-key_%s_in_%s_00' % (r, locations[l1])]]
            

    # We're done
    return (  room_links
            + no_robot_move_picking_dropping
            + only_one_robot
            + one_action_a_time
            + pick_drop_mail
            + move_mail
            + one_piece_mail
            + room_unlocks 
            + doors_stay_shut
            + key_installs
            + unique_keys
            )

#################################################################
#
# Reasoning about the world
#
#################################################################

logging.basicConfig(stream=sys.stdout, filename='complex2.log', format='%(asctime)s %(message)s', level=logging.DEBUG)
t_min = 30
t_max = 50
axioms_up_to_time_t = []
for t in xrange(0, t_max+1):
    axioms_up_to_time_t += movement_axioms(t)
    axioms = (axioms_up_to_time_t +
              [ # Start position of robot
                  ['robot_at_mail_drop_00']
                # Start position of mail
                , ['mail_at_r113_00']
                # Target position for mail at time t
                , ['mail_at_r121_%02d' % t]
                
                # Lock room with mail
                , ['locked_r121_00'], ['locked_r113_00']
                
                # robot has no keys for the locked room
                , ['-robot_has_key_r121_00'], ['-robot_has_key_r113_00']
                
                # place key for room
                , ['key_r121_in_r127_00'], ['key_r113_in_r111_00']
                
                # Give some ice cream and show love for better results
                , ['has_ice_cream_00'], ['robot_is_loved_00']

                # Robot starts empty handed
                , ['-robot_carries_mail_00']
                #, ['robot_at_ts_%02d' % t]
                # Robot has no mail at the end
                , ['-robot_carries_mail_%02d' % t]
                
              ])
    if t >= t_min:
        logging.debug('Trying to find solution in %d steps (%d axioms in %d clauses)...' % (t, len(set([axiom for clause in axioms for axiom in clause])), len(axioms)))
        solution = solve(axioms)
        if(solution != 'UNSAT'):
            logging.info("Found a solution in %d steps:" % t)
            print_actions_in(solution)
            print_atom_trace_by_step(solution)
            sys.exit(0)
logging.info("Not possible in up to %d steps. :-(" % t_max)
