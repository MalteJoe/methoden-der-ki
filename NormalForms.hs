module NormalForms
where
  
import Data.List
import PropositionalLogic

-- Remove conditionals and biconditionals from formula
removeCond :: Plf -> Plf
removeCond (Cond x y) = removeCond (Or (Not x) y)
removeCond (Bicond x y) = removeCond (And (Cond x y) (Cond y x))
removeCond (At x) = (At x)
removeCond (Not x) = Not (removeCond x)
removeCond (And x y) = And (removeCond x) (removeCond y)
removeCond (Or x y) = Or (removeCond x) (removeCond y)

-- Transfer formula in Negation Normal Form
nnf :: Plf -> Plf
nnf x = nnf' (removeCond x)
  where
  nnf' (At x) = At x
  nnf' (Not (At x)) = Not (At x)
  nnf' (Not (Not x)) = (nnf' x)
  nnf' (And x y) = And (nnf' x) (nnf' y)
  nnf' (Or x y) = Or (nnf' x) (nnf' y)
  nnf' (Not (And x y)) = nnf' (Or (Not x) (Not y))
  nnf' (Not (Or x y)) = nnf' (And (Not x) (Not y))

-- Conjunctive Normal Form
cnf :: Plf -> Plf
cnf f = cnf' $ nnf f
  where
    cnf' (Or x y) = pushInOr (Or (cnf' x) (cnf' y))
      where
        pushInOr (Or (And x y) z) = And (pushInOr (Or x z)) 
                                         (pushInOr (Or y z))
        pushInOr (Or x (And y z)) = And (pushInOr (Or x y)) 
                                         (pushInOr (Or x z))
        pushInOr x = x
    cnf' (And x y) = And (cnf' x) (cnf' y)
    cnf' x = x


-- Convert formula in CNF to clauses
clausalCnf :: Plf -> [[Plf]]
clausalCnf f = clausalCnf' $ cnf f
  where
    clausalCnf' (And x y) = (clausalCnf' x) ++ (clausalCnf' y)
    clausalCnf' (Or x y) = [(orClauses x) ++ (orClauses y)]
      where
      orClauses (Or x y) = (orClauses x) ++ (orClauses y)
      orClauses x = [x]
    clausalCnf' x = [[x]]


-- Negate an atom
negAtom :: Plf -> Plf
negAtom (Not (At a)) = At a
negAtom (At a) = Not (At a)


-- Simplify clauslCnf formulas by removing all double atoms in clauses,
-- and by removing all clauses where an atom appears both negated
-- and non-negated
simplify :: [[Plf]] -> [[Plf]]
simplify s = filter (not . atomBothPosAndNegInClause) $ map nub s
  where
    atomBothPosAndNegInClause [] = False
    atomBothPosAndNegInClause (f:r) =
      ((length r) /= (length $ filter (/= negAtom f) r))
      || (atomBothPosAndNegInClause r)


-- Transformations for Wumpus world

-- Normal case: 0 < x < worldSize - 1, 0 < x < worldSize - 1
normalCase = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "North_t-1")) `And`
                            (Not (At "South_t-1")) `And`
                            (Not (At "West_t-1")) `And`
                            (Not (At "East_t-1")))
    `Or`
     ((At "L_x+1_y_t-1") `And` (At "West_t-1")) `Or`
     ((At "L_x-1_y_t-1") `And` (At "East_t-1")) `Or`
     ((At "L_x_y-1_t-1") `And` (At "South_t-1")) `Or`
     ((At "L_x_y+1_t-1") `And` (At "North_t-1")))

-- Corner case: x = 0, y = 0
cornerCase0 = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "South_t-1")) `And`
                            (Not (At "East_t-1")))
    `Or`
     ((At "L_x+1_y_t-1") `And` (At "West_t-1")) `Or`
     ((At "L_x_y+1_t-1") `And` (At "North_t-1")))

-- Corner case: x = 0, 0 < y < worldSize - 1
cornerCase1 = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "South_t-1")) `And`
                            (Not (At "North_t-1")) `And`
                            (Not (At "East_t-1")))
    `Or`
     ((At "L_x+1_y_t-1") `And` (At "West_t-1")) `Or`
     ((At "L_x_y-1_t-1") `And` (At "South_t-1")) `Or`
     ((At "L_x_y+1_t-1") `And` (At "North_t-1")))

-- Corner case: x = 0, y = worldSize - 1
cornerCase2 = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "North_t-1")) `And`
                            (Not (At "East_t-1")))
    `Or`
     ((At "L_x+1_y_t-1") `And` (At "West_t-1")) `Or`
     ((At "L_x_y-1_t-1") `And` (At "South_t-1")))

-- Corner case: 0 < x < worldSize - 1, y = 0
cornerCase3 = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "South_t-1")) `And`
                            (Not (At "West_t-1")) `And`
                            (Not (At "East_t-1")))
    `Or`
     ((At "L_x+1_y_t-1") `And` (At "West_t-1")) `Or`
     ((At "L_x-1_y_t-1") `And` (At "East_t-1")) `Or`
     ((At "L_x_y+1_t-1") `And` (At "North_t-1")))

--  Corner case: 0 < x < worldSize - 1, y = worldSize - 1
cornerCase4 = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "North_t-1")) `And`
                            (Not (At "West_t-1")) `And`
                            (Not (At "East_t-1")))
    `Or`
     ((At "L_x+1_y_t-1") `And` (At "West_t-1")) `Or`
     ((At "L_x-1_y_t-1") `And` (At "East_t-1")) `Or`
     ((At "L_x_y-1_t-1") `And` (At "South_t-1")))

-- Corner case: x = worldSize - 1, y = 0
cornerCase5 = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "South_t-1")) `And`
                            (Not (At "West_t-1")))
    `Or`
     ((At "L_x-1_y_t-1") `And` (At "East_t-1")) `Or`
     ((At "L_x_y+1_t-1") `And` (At "North_t-1")))

-- Corner case: x = worldSize - 1, 0 < y < worldSize - 1
cornerCase6 = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "North_t-1")) `And`
                            (Not (At "South_t-1")) `And`
                            (Not (At "West_t-1")))
    `Or`
     ((At "L_x-1_y_t-1") `And` (At "East_t-1")) `Or`
     ((At "L_x_y-1_t-1") `And` (At "South_t-1")) `Or`
     ((At "L_x_y+1_t-1") `And` (At "North_t-1")))

-- Corner case: x = worldSize - 1,     y = worldSize - 1
cornerCase7 = 
  (At "L_x_y_t") `Bicond`
   (((At "L_x_y_t-1") `And` (Not (At "North_t-1")) `And`
                            (Not (At "West_t-1")))
    `Or`
     ((At "L_x-1_y_t-1") `And` (At "East_t-1")) `Or`
     ((At "L_x_y-1_t-1") `And` (At "South_t-1")))


-- Agent senses glitter
glitter =
  ((At "L_x_y") `And` (At "G_x_y")) `Bicond` (At "Glitter_t")


-- Fluent axioms regarding gold
haveGoldFluent = 
  (At "HaveGold_t") `Bicond`
  ((At "HaveGold_t-1") `Or` ((At "goldAtom") `And` (At "locationAtom_t") `And` (At "Grab_t")))


-- Translate formula in CNF to a python list represented as a string.
cnf2python :: Plf -> String
cnf2python (Or a b) =
  (cnf2python a) ++ ", " ++ (cnf2python b)
cnf2python (And a b) =
  (cnf2python a) ++ "\n" ++ (cnf2python b)
cnf2python (Not (At a)) =
  show $ "-" ++ a
cnf2python (At a) = show $ a



-- Generate output
main = do
  putStrLn("normalCase")
  putStrLn $ cnf2python $ cnf $ normalCase
  putStrLn("cornerCase0")
  putStrLn $ cnf2python $ cnf $ cornerCase0
  putStrLn("cornerCase1")
  putStrLn $ cnf2python $ cnf $ cornerCase1
  putStrLn("cornerCase2")
  putStrLn $ cnf2python $ cnf $ cornerCase2
  putStrLn("cornerCase3")
  putStrLn $ cnf2python $ cnf $ cornerCase3
  putStrLn("cornerCase4")
  putStrLn $ cnf2python $ cnf $ cornerCase4
  putStrLn("cornerCase5")
  putStrLn $ cnf2python $ cnf $ cornerCase5
  putStrLn("cornerCase6")
  putStrLn $ cnf2python $ cnf $ cornerCase6
  putStrLn("cornerCase7")
  putStrLn $ cnf2python $ cnf $ cornerCase7
  putStrLn("glitter")
  putStrLn $ cnf2python $ cnf $ glitter
  putStrLn("haveGoldFluent")
  putStrLn $ cnf2python $ cnf $ haveGoldFluent
