#Purely Logical Agent: Mail Delivery

##Assignment

> Basierend auf der Beispieldatei delivery.py implementieren Sie einen Purley Logical Agent für das Mail Delivery Beispiel. Ihre Implementierung soll das komplette Stockwerk umfassen. Weiterhin soll modelliert werden, dass Räume (nicht Labore) abgeschlossen sein können. Für abgeschlossene Räume existieren Schlüssel. Um in einen abgeschlossenen Raum zu kommen, muss der Roboter zunächst in den Raum mit dem passenden Schlüssel fahren und den Schlüssel aufnehmen. Dann kann er eine Tür aufschließen.

##Floorplan
![](floorplan.png "Floorplan")


##Implementations


##Test Results
| t~max~ | Mail Delivery Route | Keys | Time |
| ------ | -------------------:| ---- | ---- |
| 12     | r107 -> r103        |      | ~10s |